"use strict";

import Inferno, {linkEvent} from 'inferno';
import Component from 'inferno-component';

export const Input = ({type, ...props}) => <input className="input" type={type || "text"} {...props} />;

export default class GameList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: null
        };

        this.ui = {
            table: {
                heading: ["Title", "Developer", "Publisher", "Genre"]
            }
        };
    }

    handle_filter_title(self, event) {
        self.setState(() => {
            let filter = null;
            if (event.target.value) {
                filter = new RegExp(event.target.value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), 'i');
            }

            return { filter: filter };
        });
    }

    get_display_games() {
        if (this.state.filter === null) {
            return Object.keys(this.props.list);
        }
        else {
            return Object.keys(this.props.list).filter((val) => this.state.filter.test(val));
        }
    }

    render() {
        const display_games = this.get_display_games();
        return (
            <div>
                <nav className="navbar">
                    <section className="menu">
                        <a className="heading">jRPG List</a>
                        <a>About</a>
                    </section>
                    <section className="filters">
                        <Input placeholder="Game Title..." onInput={linkEvent(this, this.handle_filter_title)}/>
                    </section>
                </nav>

                <div className="table">
                    <div className="row heading">
                        {this.ui.table.heading.map((header) =>
                            <span className="cell">{header}</span>
                        )}
                    </div>
                    {display_games.map((key) =>
                        <div key={key} className="row">
                            <span className="cell">{key}</span>
                            <span className="cell">{this.props.list[key].developer}</span>
                            <span className="cell">{this.props.list[key].publisher}</span>
                            <span className="cell">{this.props.list[key].type}</span>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
