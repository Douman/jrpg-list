"use strict";

import Inferno from 'inferno';
import GameList from './components/GameList.jsx';

import list from '../data/reddit-jrpg-list.json';

Inferno.render(<GameList list={list}/>, document.body.children[0]);
