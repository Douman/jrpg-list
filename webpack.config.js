"use strict";

const webpack = require('webpack');

const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const PUBLIC = path.resolve(__dirname, 'public');

const stylus_loader = {
    test: /\.styl/,
    use: [
        'css-loader',
        {
            loader: 'stylus-loader',
            options: {
                use: [require('autoprefixer-stylus')()]
            }
        }
    ]
};

const json_loader = {
    test: /\.json$/,
    loader: 'json5-loader'
};

const img_loader = {
    test: /\.(jpe?g|png|svg|ico)$/,
    loader: 'file-loader',
    options: {
        useRelatievePath: true,
        publicPath: "./img/",
        name: '[path][name].[hash].[ext]',
    },
};

const js_loader = {
    test: /\.jsx?$/,
    exclude: /node_modules/,
    use: [
        "babel-loader",
        "eslint-loader"
    ]
};

module.exports.entry = {
    data: './data/reddit-jrpg-list.json',
    app: './src/init.js'
};

module.exports.output = {
    filename: '[name].js',
    path: PUBLIC
};

module.exports.module = {
    rules: [
        stylus_loader,
        img_loader,
        json_loader,
        js_loader
    ]
};

function html_pug_plug(title, template) {
    return new HtmlWebpackPlugin({
        title,
        template: `!!pug-loader!${template}`,
    });
}

function common_chunk(name, minChunks) {
    return new webpack.optimize.CommonsChunkPlugin({
        name,
        minChunks
    })
}

module.exports.plugins = [
    html_pug_plug("jRPG list", "src/templates/index.pug"),
    common_chunk('data', Infinity)
];

module.exports.devServer = {
    port: 3333,
    compress: true
};

module.exports.devtool = "cheap-source-map";
