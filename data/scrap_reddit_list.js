"use strict";

const http = require('https');
const path = require('path');
const fs = require('fs');

const cheerio = require('cheerio');

const URL = "https://www.reddit.com/r/JRPG/wiki/complete_list_of_jrpgs";
const RESULT_FILE = path.join(__dirname, "reddit-jrpg-list.json");

function write_result(data) {
    fs.writeFile(RESULT_FILE, JSON.stringify(data, null, 4), 'utf-8', () => {
        console.log("Downloaded JRPG list from reddit -> %s", RESULT_FILE);
    });
}

function get_headers(data) {
    const result = [];
    data.each((_, element) => {
        result.push(element.children[0].data);
    });
    result.push("Steam");
    result.push("Steam");
    return result;
}

function get_cell_date(cell) {
    if (cell.children === void 0 || cell.children.length === 0) return null;

    if (cell.children.length > 1) return cell.children[1].data.trim();
    else return cell.children[0].data;
}

function get_cell_text(cell) {
    return cell.children === void 0 ? null : cell.children.length === 0 ? null : cell.children[0].data;
}

function parse_row(row) {
    const cells = row.children();

    if (cells.length < 5) return null;

    let title;
    let url;
    if (cells[0].children[0].type === 'tag') {
        title = cells[0].children[0].children[0].data;
        url = cells[0].children[0].attribs.href;
    }
    else {
        title = cells[0].children[0].data;
        url = null;
    }
    return {
        title: title,
        url: url,
        date: get_cell_date(cells[1]),
        developer: get_cell_text(cells[2]),
        publisher: get_cell_text(cells[3]),
        //For PC there is extra OS column
        type: cells.length === 7 ? get_cell_text(cells[5]) : get_cell_text(cells[4])
    };
}

function parse_data(data) {
    const html = cheerio.load(data);
    const headers = get_headers(html('h4'));
    const tables = html('table > tbody');
    const jrpg_list = {};

    tables.each((idx, table) => {
        html(table).find('tr').each((_, row) => {
            row = html(row);
            const result = parse_row(html(row));

            if (result !== null) {
                if (result.title in jrpg_list) {
                    jrpg_list[result.title].url.push(result.url);
                    jrpg_list[result.title].platform.push(headers[idx]);
                    jrpg_list[result.title].date.push(result.date);
                }
                else {
                    jrpg_list[result.title] = {
                        date: [result.date],
                        url: [result.url],
                        developer: result.developer,
                        publisher: result.publisher,
                        type: result.type,
                        platform: [headers[idx]]
                    };
                }
            }
        });
    });


    write_result(jrpg_list);
}

function handle_response(response) {
    if (response.statusCode < 200 && response.statusCode > 299) {
        console.log("Cannot access JRPG list. Status code='%d'", response.statusCode);
        return;
    }

    response.setEncoding('utf8');
    let data = '';

    response.on('data', (chunk) => {
        data += chunk;
    });
    response.on('end', () => {
        parse_data(data);
    });
}

function error_handle(error) {
    console.error("Got error: " + error.message);
}

http.get(URL, handle_response).on("error", error_handle);

